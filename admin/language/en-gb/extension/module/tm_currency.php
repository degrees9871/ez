<?php
// Heading
$_['heading_title']    = 'TemplateMonster Currency';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified TemplateMonster Currency module!';
$_['text_edit']        = 'Edit TemplateMonster Currency Module';

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify TemplateMonster Currency module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';