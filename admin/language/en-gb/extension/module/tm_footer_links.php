<?php
// Heading
$_['heading_title']    = 'TemplateMonster Footer Links';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified TemplateMonster Footer Links module!';
$_['text_edit']        = 'Edit TemplateMonster Footer Links Module';
$_['text_groups']      = ['Information', 'Customer Service', 'Extras', 'My Account', 'Contact Us', 'Ways to shop'];

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_status']     = 'Status';
$_['entry_group']      = 'Links Group';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify TemplateMonster Footer Links module!';
$_['error_name']       = 'Module Name must be between 3 and 64 characters!';