<?php
// Heading
$_['heading_title']    = 'WYSIWYG редакторы';

// Text
$_['text_extension']   = 'Модули';
$_['text_success']     = 'Настройки модуля обновлены!';
$_['text_edit']        = 'Редактирование модуля';

// Entry
$_['entry_status']     = 'Статус';
$_['entry_editor']     = 'Редактор';
$_['entry_modificator']     = 'Модификатор';
$_['entry_download']     = 'Скачать';
$_['entry_uploadmethod']     = 'Способ загрузки файлов';
$_['force_method']     = 'Минуя окно файлового менеджера';
$_['standart_method']     = 'Стандартный файловый менеджер';
$_['elfinder_method']     = 'Файловый менеджер Elfinder';
$_['entry_uploaddir']     = 'Директория загрузки файлов';
$_['help_uploaddir']     = 'Директория указывается относительно директории image/catalog/';

// Error
$_['error_permission'] = 'У вас нет прав для управления этим модулем!';
