<?php
// Heading
$_['heading_title']        = 'TemplateMonster Module Tabs';

// Text
$_['text_module']          = 'Модули';
$_['text_success']         = 'Успешно: Вы изменили настройки модуля TemplateMonster Module Tabs!';
$_['text_edit']            = 'Редактировать модуль TemplateMonster Module Tabs';
$_['text_layout_carousel'] = 'Карусель';
$_['text_layout_static']   = 'Статический';

// Entry
$_['entry_name']           = 'Название модуля';
$_['entry_product']        = 'Избранные продукты';
$_['entry_featured']       = 'Избранное';
$_['entry_special']        = 'Скидки';
$_['entry_bestseller']     = 'Самые популярные';
$_['entry_latest']         = 'Последнее';
$_['entry_limit']          = 'Ограничение';
$_['entry_width']          = 'Ширина';
$_['entry_height']         = 'Высота';
$_['entry_status']         = 'Статус';
$_['entry_layout_type']    = 'Тип макета';

// Help
$_['help_product']         = '(Автозаполнение)';

// Error
$_['error_permission']     = 'У вас нет разрешения на редактирование модуля TemplateMonster Module Tabs!';
$_['error_name']           = 'Название модуля должно содержать от 3х до 64х символов!';
$_['error_width']          = 'Необходимо указать ширину!';
$_['error_height']         = 'Необходимо указать высоту!';