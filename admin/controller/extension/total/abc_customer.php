<?php
class ControllerExtensionTotalAbcCustomer extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/total/abc_customer');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
			$this->model_setting_setting->editSetting('abc_customer', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');

        $data['text_edit'] = $this->language->get('heading_title');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_discount'] = $this->language->get('entry_discount');
		$data['entry_discount_help'] = $this->language->get('entry_discount_help');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'] . '&type=module', true),
			'separator' => false
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_total'),
			'href'      => $this->url->link('extension/extension/total', 'token=' . $this->session->data['token'] . '&type=module', true),
			'separator' => ' :: '
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/total/abc_customer', 'token=' . $this->session->data['token'] . '&type=module', true),
			'separator' => ' :: '
		);

		$data['action'] = $this->url->link('extension/total/abc_customer', 'token=' . $this->session->data['token'] . '&type=module', true);

		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true);

		if (isset($this->request->post['abc_customer_status'])) {
			$data['abc_customer_status'] = $this->request->post['abc_customer_status'];
		} else {
			$data['abc_customer_status'] = $this->config->get('abc_customer_status');
		}

		if (isset($this->request->post['abc_customer_sort_order'])) {
			$data['abc_customer_sort_order'] = $this->request->post['abc_customer_sort_order'];
		} else {
			$data['abc_customer_sort_order'] = $this->config->get('abc_customer_sort_order');
		}

		$this->load->model('customer/customer_group');
		$customer_groups = $this->model_customer_customer_group->getCustomerGroups(array('sort' => 'cg.sort_order'));
		$discounts = $this->config->get('abc_customer_group_id');

		foreach($customer_groups as $key => $group){
			if(isset($discounts[$group['customer_group_id']])){
				$customer_groups[$key]['discount'] = $discounts[$group['customer_group_id']];
			}else{
				$customer_groups[$key]['discount'] = 0;
			}
		}

		$data['customer_groups'] = $customer_groups;


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/total/abc_customer.tpl', $data));
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'extension/total/abc_customer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

}
?>