<?php
class ControllerExtensionModuleWysiwygText extends Controller {
	private $error = array();

	public function index() {
		
		$this->load->language('extension/module/wysiwygtext');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('wysiwygtext', $this->request->post);
			

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_editor'] = $this->language->get('entry_editor');
		$data['entry_modificator'] = $this->language->get('entry_modificator');
		$data['entry_download'] = $this->language->get('entry_download');
		$data['entry_uploadmethod'] = $this->language->get('entry_uploadmethod');
		$data['entry_uploaddir'] = $this->language->get('entry_uploaddir');
		$data['help_uploaddir'] = $this->language->get('help_uploaddir');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		
		$data['modificatorTinyMce'] = $this->url->link('extension/module/wysiwygtext/modificatorTinyMce', 'token=' . $this->session->data['token'], 'SSL');
		$data['modificatorCkEditor'] = $this->url->link('extension/module/wysiwygtext/modificatorCkEditor', 'token=' . $this->session->data['token'], 'SSL');
		
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/wysiwygtext', 'token=' . $this->session->data['token'], true)
		);

		if (!isset($this->request->get['module_id'])) {
			$data['action'] = $this->url->link('extension/module/wysiwygtext', 'token=' . $this->session->data['token'], true);
		} else {
			$data['action'] = $this->url->link('extension/module/wysiwygtext', 'token=' . $this->session->data['token'] . '&module_id=' . $this->request->get['module_id'], true);
		}
		
		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true);
		
		if (isset($this->request->post['wysiwygtext_status'])) {
			$data['wysiwygtext_status'] = $this->request->post['wysiwygtext_status'];
		} else {
			$data['wysiwygtext_status'] = $this->config->get('wysiwygtext_status');
		}
		
		if (isset($this->request->post['wysiwygtext_editor'])) {
			$data['wysiwygtext_editor'] = $this->request->post['wysiwygtext_editor'];
		} else {
			$data['wysiwygtext_editor'] = $this->config->get('wysiwygtext_editor');
		}
		
		$data['uploadmethods'] = array('standart' => $this->language->get('standart_method'),
										'force' => $this->language->get('force_method'));
		
		if (isset($this->request->post['wysiwygtext_uploadmethod'])) {
			$data['wysiwygtext_uploadmethod'] = $this->request->post['wysiwygtext_uploadmethod'];
		} else {
			$data['wysiwygtext_uploadmethod'] = $this->config->get('wysiwygtext_uploadmethod');
		}
		
		if (isset($this->request->post['wysiwygtext_uploaddir'])) {
			$data['wysiwygtext_uploaddir'] = $this->request->post['wysiwygtext_uploaddir'];
		} else {
			$data['wysiwygtext_uploaddir'] = $this->config->get('wysiwygtext_uploaddir');
		}


		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('extension/module/wysiwygtext', $data));
	}

	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/wysiwygtext')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		return !$this->error;
	}
}