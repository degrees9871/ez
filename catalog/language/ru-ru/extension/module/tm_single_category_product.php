<?php
// Heading
$_['heading_title']       = 'Tm Module Tabs';
$_['heading_latest']      = 'Новые';
$_['heading_specials']    = 'Скидки';
$_['heading_featured']    = 'Избранное';
$_['heading_bestsellers'] = 'Популярные';

// Text
$_['text_tax']            = 'Без НДС:';
$_['text_sale']           = 'Скидка!';
$_['text_new']            = 'Новинка!';
$_['text_view']           = 'Просмотреть все продукты';
$_['text_option']         = 'Доступные варианты';
$_['text_select']         = '--- Пожалуйста, выберите ---';

$_['text_category']       = 'Категории';
$_['text_manufacturer']   = 'Торговая марка:';
$_['text_model']          = 'Модель:';
$_['text_availability']   = 'Наличие:';
$_['text_instock']        = 'На складе';
$_['text_outstock']       = 'Нет на складе';
$_['text_price']          = 'Цена: ';
$_['text_tax']            = 'Без НДС:';
$_['text_quick']          = 'Быстрый просмотр';
$_['button_details']      = 'Подробности';
$_['reviews']             = 'отзывы';
$_['text_product']        = 'Продукт {current} из {total} ';
