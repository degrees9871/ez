<?php

// Heading 
$_['heading_title']        = 'Блог';

$_['text_date_format']     = 'd/m/Y';

$_['text_popular_all']     = 'Популярные статьи';
$_['text_latest_all']      = 'Последние статьи';
$_['text_button_continue'] = 'Подробнее';
$_['text_comments']        = 'Комментарии';
$_['text_comment']         = 'Комментарий';



$_['text_no_result']       = 'Нет результатов!';

?>