<?php
// Heading
$_['heading_title']    = 'Особистий кабінет';

// Text
$_['text_register']    = 'Реєстрация';
$_['text_login']       = 'Вхід';
$_['text_logout']      = 'Вихід';
$_['text_forgotten']   = 'Забули пароль?';
$_['text_account']     = 'Моя информація';
$_['text_edit']        = 'Змінити контактну інформацию';
$_['text_password']    = 'Пароль';
$_['text_address']     = 'Адресна книга';
$_['text_wishlist']    = 'Закладки';
$_['text_order']       = 'Істория замовлень';
$_['text_download']    = 'Файли для скачувания';
$_['text_reward']      = 'Бонусні бали';
$_['text_return']      = 'Повернення';
$_['text_transaction'] = 'Історія транзакцій';
$_['text_newsletter']  = 'E-Mail рассилка';
$_['text_recurring']   = 'Регулярні платежі';

