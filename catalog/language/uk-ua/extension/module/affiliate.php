<?php
// Heading
$_['heading_title']    = 'Партнерський разділ';

// Text
$_['text_register']    = 'Реєстрація';
$_['text_login']       = 'Вхід';
$_['text_logout']      = 'Вихід';
$_['text_forgotten']   = 'Забули пароль?';
$_['text_account']     = 'Моя інформація';
$_['text_edit']        = 'Редактувати персональну інформацію';
$_['text_password']    = 'Пароль';
$_['text_payment']     = 'Способи оплати';
$_['text_tracking']    = 'Реферальний код';
$_['text_transaction'] = 'Транзакциї';


