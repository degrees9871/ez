<?php

// Heading 
$_['heading_title']        = 'Блог';

$_['text_date_format']     = 'd/m/Y';

$_['text_popular_all']     = 'Популярні статті';
$_['text_latest_all']      = 'Останні статті';
$_['text_button_continue'] = 'Детальніше';
$_['text_comments']        = ' Коментарі';
$_['text_comment']         = ' Коментар';



$_['text_no_result']       = 'Немає результатів!';

?>