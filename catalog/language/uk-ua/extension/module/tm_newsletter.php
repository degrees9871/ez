<?php
// Heading
$_['heading_title']       = 'Підписка';

// Entry
$_['entry_mail']          = 'Email';

// Text
$_['button_subscribe']    = 'Підписатися';
$_['text_success']        = 'Ви успішно підписалися';

//Errors
$_['error_exist_user']    = 'Користувач вже зареєстрований';
$_['error_exist_email']   = 'Зазначена адреса електронної пошти вже зареєстрований';
$_['error_invalid_email'] = 'Будь ласка, введіть правильну адресу електронної пошти!';