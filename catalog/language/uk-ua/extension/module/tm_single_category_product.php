<?php
// Heading
$_['heading_title']       = 'Tm Module Tabs';
$_['heading_latest']      = 'Нові';
$_['heading_specials']    = 'Знижки';
$_['heading_featured']    = 'Вибране';
$_['heading_bestsellers'] = 'Популярні';

// Text
$_['text_tax']            = 'Без ПДВ:';
$_['text_sale']           = 'Знижка!';
$_['text_new']            = 'Новинка!';
$_['text_view']           = 'Переглянути всю продукцію';
$_['text_option']         = 'Доступні варіанти';
$_['text_select']         = '--- Будь ласка, оберіть ---';

$_['text_category']       = 'Категорії';
$_['text_manufacturer']   = 'Торгова марка:';
$_['text_model']          = 'Модель:';
$_['text_availability']   = 'Наявність:';
$_['text_instock']        = 'На складі';
$_['text_outstock']       = 'Нема на складі';
$_['text_price']          = 'Ціна: ';
$_['text_tax']            = 'Без ПДВ:';
$_['text_quick']          = 'Швидкий перегляд';
$_['text_save']           = 'Зберегти';
$_['button_details']      = 'Подробиці';
$_['reviews']             = 'відгуки';
$_['text_product']        = 'Продукт {current} з {total} ';
