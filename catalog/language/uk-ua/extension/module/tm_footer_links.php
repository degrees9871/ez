<?php
// Text
$_['text_information']  = 'Інформація';
$_['text_service']      = 'Обслуговування клієнтів';
$_['text_extra']        = 'Додатково';
$_['text_contact']      = "Зворотній зв'язок";
$_['text_return']       = 'Повернення';
$_['text_sitemap']      = 'Карта сайту';
$_['text_manufacturer'] = 'Торгові марки';
$_['text_voucher']      = 'Подарункові сертифікати';
$_['text_affiliate']    = 'Партнери';
$_['text_special']      = 'Знижки';
$_['text_account']      = 'Обліковий запис';
$_['text_order']        = 'Історія замовлень';
$_['text_wishlist']     = 'Закладки';
$_['text_newsletter']   = 'Підписка';
$_['text_powered']      = 'Зроблено на <a href="http://www.opencart.com">OpenCart</a><br /> <span class="hidden">%s</span> &copy; %s';
$_['text_telephone']    = 'телефон:';
$_['text_fax']          = 'факс:';
$_['text_email']        = 'e-mail:';
$_['text_simple_blog']  = 'Блог';
$_['text_catalog']      = 'Католог продуктів';
$_['text_more']         = 'Інші способи';
$_['text_store']        = 'Знайти в магазині';
$_['text_gift_cards']   = 'Подарункові карти';
$_['text_find_wishlist']     = 'Знайти в закладках';