<?php
// Heading 
$_['heading_title']					= 'Блог';

// Buttons
$_['button_continue_reading']		= 'Читати далі';
$_['button_submit']					= 'Відправити коментарь';

$_['text_date_format']				= 'd/m/Y';
$_['text_date_format_long']			= 'd/m/Y  g:i A';

// Entry
$_['entry_name']					= "Ваше ім'я:";
$_['entry_captcha']					= 'Введіть код в поле нижче:';
$_['entry_review']					= 'Ваший комментарь:';

// Text
$_['text_no_found']					= 'Нема створиних блогів!';
$_['text_related_product']			= 'Схожі товари';
$_['text_related_comment']			= 'Коментар';
$_['text_related_article']			= 'Схожі статті';
$_['text_author_information']		= 'Автор';
$_['text_posted_by']				= 'Опубліковано';
$_['text_updated']					= 'Оновлення на';
$_['text_comment_on_article']		= 'Коментарі';
$_['text_view_comment']				= 'Переглянути коментарі';
$_['text_write_comment']			= 'Залиште свій коментар';
$_['text_cancel_reply']			    = 'Відповісти на питання';
$_['text_note']						= 'Примітка: HTML не перекладається';
$_['text_comments']					= ' Коментарі';
$_['text_comment']					= ' Коментар';
$_['text_no_blog']   				= 'Відсутні коментарі для цього блога.';
$_['text_on']           			= ' на ';
$_['text_success']      			= 'Дякуємо за Ваш коментар!';
$_['text_success_approval']			= 'Дякуємо за ваш коментар. 
Він був преданий на веб-майстру для затвердження!';
$_['text_wait']						= 'Очікання';
$_['text_reply_comment']			= 'Відповідь';
$_['text_said']						= 'відповів:';
$_['text_authors']					= 'Автори';

$_['text_category_error']			= 'Категорії блога не знайдені!';
$_['text_author_error']				= 'Автор блога не знайдений!';
$_['text_article_error']			= 'Блог не знайдений!';

// Error
$_['error_name']        			= "Попередження: Ім'я автора повинно бути від 3 до 25 символів!";
$_['error_text']        			= 'Попередження: Текст коментаря повинен бути від 3 до 1000 символів';
$_['error_captcha']     			= 'Увага: перевірка не пройдена';


?>