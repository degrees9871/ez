<script>
	<?php if ($tabs == '1') {?>
		$(document).ready(function ($) {
			$('#module-single-tabs-<?php echo $module; ?> a:first').tab('show')
			
		});
		<?php }else{?>
			$(document).ready(function ($) {
				$('#single-category<?php echo $module; ?> .tab-content > .tab-pane').css({
					'display': 'block',
					'visibility': 'visible'
				});
			});
			<?php }?>
		</script>

		<div class="box single-category">
			<div class="box-heading"><h3><?php echo $category_name ?></h3></div>
			<div class="box-content">
				<div role="tabpanel" class="module_tab" id="single-category<?php echo $module; ?>">
					<div class="box-heading"><h3><?php echo $category_name ?></h3></div>
					<?php if ($tabs == '1') { ?>
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist" id="module-single-tabs-<?php echo $module; ?>">	
						<?php if ($latest_products) { ?>
						<li><a href="#tab-single-latest-<?php echo $module; ?>" role="tab" data-toggle="tab"><?php echo $heading_latest; ?></a></li>
						<?php } ?>	
						<?php if ($bestseller_products) { ?>
						<li><a href="#tab-single-bestsellers-<?php echo $module; ?>" role="tab" data-toggle="tab"><?php echo $heading_bestsellers; ?></a></li>
						<?php } ?>
						<?php if ($featured_products) { ?>
						<li><a href="#tab-single-featured-<?php echo $module; ?>" role="tab" data-toggle="tab"><?php echo $heading_featured; ?></a></li>
						<?php } ?>
						<?php if ($special_products) { ?>
						<li><a href="#tab-single-specials-<?php echo $module; ?>" role="tab" data-toggle="tab"><?php echo $heading_specials; ?></a></li>
						<?php } ?>
					</ul>
					<?php } else { ?>					
					<?php if ($latest_products) { ?><h3><?php echo $heading_latest; ?></h3><?php } ?>					
					<?php if ($bestseller_products) { ?><h3><?php echo $heading_bestsellers; ?></h3><?php } ?>
					<?php if ($featured_products) { ?><h3><?php echo $heading_featured; ?></h3><?php } ?>
					<?php if ($special_products) { ?><h3><?php echo $heading_specials; ?></h3><?php } ?>
					<?php } ?>
					<div class="clear"></div>
					<!-- Tab panes -->
					<div class="tab-content">						
						<?php if ($latest_products) { ?>
						<div role="tabpanel" class="tab-pane" id="tab-single-latest-<?php echo $module; ?>">
							<div class="box clearfix">
								<div class="<?php echo $layout_type ? 'box-carousel' : 'row mobile-carousel'; ?>">
									<?php $tl = 6000; $clr = 0;
									foreach ($latest_products as $product) { $tl++; $clr++; ?>
									<?php if ($layout_type == 0) { ?>
									<div class="product-layout col-lg-3 col-md-3 col-sm-3 col-xs-12" <?php echo $clr%4 == 0 ? 'data-clear=""' : ''; ?>>
										<?php } ?>
										<div class="product-thumb transition <?php if ($product['options']) echo 'options'; ?>">
											<?php if ($product['options']) { ?>
											<!-- Product options -->
											<div class="product-option-wrap">
												<div class="product-options form-horizontal">
													<div class="options">
														<a class="ajax-overlay_close" href='#'></a>
														<h3><?php echo $text_option; ?></h3>
														<div class="form-group hidden">
															<div class="col-sm-8">
																<input type="text" name="product_id" value="<?php echo $product['product_id'] ?>" class="form-control"/>
															</div>
														</div>
														<?php foreach ($product['options'] as $option) { ?>
														<?php if ($option['type'] == 'select') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] .  $module . $tl; ?>">
																<?php echo $option['name']; ?>
															</label>
															<div class="col-sm-12">
																<select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id'] .  $module . $tl; ?>" class="form-control">
																	<option value=""><?php echo $text_select; ?></option>
																	<?php foreach ($option['product_option_value'] as $option_value) { ?>
																	<option value="<?php echo $option_value['product_option_value_id']; ?>">
																		<?php echo $option_value['name']; ?>
																		<?php if ($option_value['price']) { ?>
																		(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
																		<?php } ?>
																	</option>
																	<?php } ?>
																</select>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'radio') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<div id="input-option<?php echo $option['product_option_id'] .  $module . $tl; ?>">
																	<?php foreach ($option['product_option_value'] as $option_value) { ?>
																	<div class="radio">
																		<label for="option[<?php echo $option['product_option_id'] . $option_value['product_option_value_id'] .  $module . $tl; ?>]">
																			<input type="radio" hidden name="option[<?php echo $option['product_option_id']; ?>]" id="option[<?php echo $option['product_option_id'] . $option_value['product_option_value_id'] .  $module . $tl; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>"/>
																			<?php echo $option_value['name']; ?>
																			<?php if ($option_value['price']) { ?>
																			(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
																			<?php } ?>
																		</label>
																	</div>
																	<?php } ?>
																</div>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'checkbox') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<div id="input-option<?php echo $option['product_option_id'] .  $module . $tl; ?>">
																	<?php foreach ($option['product_option_value'] as $option_value) { ?>
																	<div class="checkbox">
																		<label>
																			<input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>"/>
																			<?php echo $option_value['name']; ?>
																			<?php if ($option_value['price']) { ?>
																			(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
																			<?php } ?>
																		</label>
																	</div>
																	<?php } ?>
																</div>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'image') { ?>
														<div class="option-color form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<div id="input-option<?php echo $option['product_option_id'] .  $module . $tl; ?>">
																	<?php foreach ($option['product_option_value'] as $option_value) { ?>
																	<div class="radio">
																		<label>
											<input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
											<img width="21" height="21" data-toggle="tooltip" title="<?php echo $option_value['name']; ?><?php if ($option_value['price']) { ?> (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>) <?php } ?>" src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" />
										</label>
																	</div>
																	<?php } ?>
																</div>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'text') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] .  $module . $tl; ?>"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id'] .  $module . $tl; ?>" class="form-control"/>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'textarea') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] .  $module . $tl; ?>"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id'] .  $module . $tl; ?>" class="form-control">
																	<?php echo $option['value']; ?>
																</textarea>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'file') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12 "><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<button type="button" id="button-upload<?php echo $option['product_option_id'] .  $module . $tl; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-block btn-default">
																	<i class="fa fa-upload"></i>
																	<?php echo $button_upload; ?>
																</button>
																<input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id'] .  $module . $tl; ?>"/>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'date') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] .  $module . $tl; ?>"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<div class="input-group date">
																	<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id'] .  $module . $tl; ?>" class="form-control"/>
																	<span class="input-group-btn">
																		<button class="btn btn-default" type="button">
																			<i class="fa fa-calendar"></i>
																		</button>
																	</span>
																</div>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'datetime') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] .  $module . $tl; ?>"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<div class="input-group datetime">
																	<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id'] .  $module . $tl; ?>" class="form-control"/>
																	<span class="input-group-btn">
																		<button type="button" class="btn btn-default">
																			<i class="fa fa-calendar"></i>
																		</button>
																	</span>
																</div>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'time') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] .  $module . $tl; ?>">
																<?php echo $option['name']; ?>
															</label>
															<div class="col-sm-12">
																<div class="input-group time">
																	<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id'] .  $module . $tl; ?>" class="form-control"/>
																	<span class="input-group-btn">
																		<button type="button" class="btn btn-default">
																			<i class="fa fa-calendar"></i>
																		</button>
																	</span>
																</div>
															</div>
														</div>
														<?php } ?>
														<?php } ?>
														<button class="btn-primary" type="button" onclick="cart.addPopup($(this),'<?php echo $product['product_id']; ?>');"><?php echo $button_cart; ?></button>
													</div>
												</div>
											</div>
											<?php } ?>											
											
											<div class="image">
												<a class="lazy" style="padding-bottom: <?php echo($product['img-height'] / $product['img-width'] * 100); ?>%" href="<?php echo $product['href']; ?>">
													<?php if ($product['additional_thumb']) { ?>
													<img width="<?php echo $product['img-width']; ?>" height="<?php echo $product['img-height'] ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-primary" data-src="<?php echo $product['thumb']; ?>" src="#"/>
													<img width="<?php echo $product['img-width']; ?>" height="<?php echo $product['img-height'] ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-secondary" data-src="<?php echo $product['additional_thumb']; ?>" src="#"/>
													<?php } else { ?>
													<img width="<?php echo $product['img-width']; ?>" height="<?php echo $product['img-height'] ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img" data-src="<?php echo $product['thumb']; ?>" src="#"/>
													<?php } ?>
												</a>
												<?php if ($product['rating']) { ?>
												<div class="rating">
													<?php for ($i = 1; $i <= 5; $i++) { ?>
													<?php if ($product['rating'] < $i) { ?>
													<span class="fa-stack"><i class="material-icons-star fa-stack-1x"></i></span>
													<?php } else { ?>
													<span class="fa-stack"><i class="material-icons-star star fa-stack-1x"></i></span>
													<?php } ?>
													<?php } ?>
												</div>
												<?php } ?>
												<?php if ($product['special']) { ?>
												<?php if ($label_sale) { ?>
												<div class="sale">
													<span><?php echo $text_sale; ?></span>
												</div>
												<?php } ?>
												<?php if ($label_discount) { ?>
												<div class="discount">
													<span><?php echo $product['label_discount']; ?></span>
												</div>
												<?php } ?>
												<?php } ?>
												<?php if ($product['label_new']) { ?>
												<div class="new-pr"><span><?php echo $text_new; ?></span></div>
												<?php } ?>
											</div>
											<div class="caption">
												<div class="name">
													<a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
												</div>
												<div class="description">
													<?php if(strlen($product['description']) > 120) { echo substr($product['description'], 0, 120) . '..'; } else { echo $product['description']; } ?>
												</div>
												<?php if ($product['price']) { ?>
												<div class="price">
													<?php if (!$product['special']) { ?>
													<?php echo $product['price']; ?>
													<?php } else { ?>							 
													<span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
													<?php } ?>
													<?php if ($product['tax']) { ?>
													<span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
													<?php } ?>
												</div>
												<?php } ?>												
												<div class="cart-button">
													<button class="btn-icon btn-add" type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" <?php if (count($product['options']) > 3) { ?> onclick="cart.add('<?php echo $product['product_id']; ?>');" <?php } else { ?> onclick="ajaxAdd($(this),<?php echo $product['product_id'] ?>);" <?php } ?>><i class="material-icons-shopping_cart"></i></button>
													<button class="btn-icon" type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart-o"></i></button>
													<a href="index.php?route=extension/module/tm_ajax_quick_view/ajaxQuickView" data-rel="details" data-product="<?php echo $product['product_id']; ?>" data-image-width="<?php echo $product['img-width']; ?>" data-image-height="<?php echo $product['img-height']; ?>" class="quickview btn-icon" data-toggle="tooltip" title="<?php echo $text_quick; ?>"><i class="material-icons-visibility"></i></a>
													<button class="btn-icon" type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="material-icons-repeat"></i></button>
												</div>
											</div>
										</div>
										<?php if ($layout_type == 0) { ?>
									</div>
									<?php } ?>
									<?php } ?>
								</div>
							</div>
						</div>
						<?php } ?>						
						<?php if ($bestseller_products) { ?>
						<div role="tabpanel" class="tab-pane" id="tab-single-bestsellers-<?php echo $module; ?>">
							<div class="box clearfix">
								<div class="<?php echo $layout_type ? 'box-carousel' : 'row mobile-carousel'; ?>">
									<?php $tb = 8000; $clr = 0;
									foreach ($bestseller_products as $product) { $tb++; $clr++; ?>
									<?php if ($layout_type == 0) { ?>
									<div class="product-layout col-lg-3 col-md-3 col-sm-3 col-xs-12" <?php echo $clr%4 == 0 ? 'data-clear=""' : ''; ?>>
										<?php } ?>
										<div class="product-thumb transition <?php if ($product['options']) echo 'options'; ?>">
											<?php if ($product['options']) { ?>
											<!-- Product options -->
											<div class="product-option-wrap">
												<div class="product-options form-horizontal">
													<div class="options">
														<a class="ajax-overlay_close" href='#'></a>
														<h3><?php echo $text_option; ?></h3>
														<div class="form-group hidden">
															<div class="col-sm-8">
																<input type="text" name="product_id" value="<?php echo $product['product_id'] ?>" class="form-control"/>
															</div>
														</div>
														<?php foreach ($product['options'] as $option) { ?>
														<?php if ($option['type'] == 'select') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] .  $module . $tb; ?>">
																<?php echo $option['name']; ?>
															</label>
															<div class="col-sm-12">
																<select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id'] .  $module . $tb; ?>" class="form-control">
																	<option value=""><?php echo $text_select; ?></option>
																	<?php foreach ($option['product_option_value'] as $option_value) { ?>
																	<option value="<?php echo $option_value['product_option_value_id']; ?>">
																		<?php echo $option_value['name']; ?>
																		<?php if ($option_value['price']) { ?>
																		(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
																		<?php } ?>
																	</option>
																	<?php } ?>
																</select>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'radio') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<div id="input-option<?php echo $option['product_option_id'] .  $module . $tb; ?>">
																	<?php foreach ($option['product_option_value'] as $option_value) { ?>
																	<div class="radio">
																		<label for="option[<?php echo $option['product_option_id'] . $option_value['product_option_value_id'] .  $module . $tb; ?>]">
																			<input type="radio" hidden name="option[<?php echo $option['product_option_id']; ?>]" id="option[<?php echo $option['product_option_id'] . $option_value['product_option_value_id'] .  $module . $tb; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>"/>
																			<?php echo $option_value['name']; ?>
																			<?php if ($option_value['price']) { ?>
																			(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
																			<?php } ?>
																		</label>
																	</div>
																	<?php } ?>
																</div>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'checkbox') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<div id="input-option<?php echo $option['product_option_id'] .  $module . $tb; ?>">
																	<?php foreach ($option['product_option_value'] as $option_value) { ?>
																	<div class="checkbox">
																		<label>
																			<input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>"/>
																			<?php echo $option_value['name']; ?>
																			<?php if ($option_value['price']) { ?>
																			(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
																			<?php } ?>
																		</label>
																	</div>
																	<?php } ?>
																</div>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'image') { ?>
														<div class="option-color form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<div id="input-option<?php echo $option['product_option_id'] .  $module . $tb; ?>">
																	<?php foreach ($option['product_option_value'] as $option_value) { ?>
																	<div class="radio">
																		<label>
											<input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
											<img width="21" height="21" data-toggle="tooltip" title="<?php echo $option_value['name']; ?><?php if ($option_value['price']) { ?> (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>) <?php } ?>" src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" />
										</label>
																	</div>
																	<?php } ?>
																</div>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'text') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] .  $module . $tb; ?>"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id'] .  $module . $tb; ?>" class="form-control"/>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'textarea') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] .  $module . $tb; ?>"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id'] .  $module . $tb; ?>" class="form-control">
																	<?php echo $option['value']; ?>
																</textarea>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'file') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12 "><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<button type="button" id="button-upload<?php echo $option['product_option_id'] .  $module . $tb; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-block btn-default">
																	<i class="fa fa-upload"></i>
																	<?php echo $button_upload; ?>
																</button>
																<input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id'] .  $module . $tb; ?>"/>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'date') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] .  $module . $tb; ?>"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<div class="input-group date">
																	<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id'] .  $module . $tb; ?>" class="form-control"/>
																	<span class="input-group-btn">
																		<button class="btn btn-default" type="button">
																			<i class="fa fa-calendar"></i>
																		</button>
																	</span>
																</div>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'datetime') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] .  $module . $tb; ?>"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<div class="input-group datetime">
																	<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id'] .  $module . $tb; ?>" class="form-control"/>
																	<span class="input-group-btn">
																		<button type="button" class="btn btn-default">
																			<i class="fa fa-calendar"></i>
																		</button>
																	</span>
																</div>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'time') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] .  $module . $tb; ?>">
																<?php echo $option['name']; ?>
															</label>
															<div class="col-sm-12">
																<div class="input-group time">
																	<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id'] .  $module . $tb; ?>" class="form-control"/>
																	<span class="input-group-btn">
																		<button type="button" class="btn btn-default">
																			<i class="fa fa-calendar"></i>
																		</button>
																	</span>
																</div>
															</div>
														</div>
														<?php } ?>
														<?php } ?>
														<button class="btn-primary" type="button" onclick="cart.addPopup($(this),'<?php echo $product['product_id']; ?>');"><?php echo $button_cart; ?></button>
													</div>
												</div>
											</div>
											<?php } ?>											
											
											<div class="image">
												<a class="lazy" style="padding-bottom: <?php echo($product['img-height'] / $product['img-width'] * 100); ?>%" href="<?php echo $product['href']; ?>">
													<?php if ($product['additional_thumb']) { ?>
													<img width="<?php echo $product['img-width']; ?>" height="<?php echo $product['img-height'] ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-primary" data-src="<?php echo $product['thumb']; ?>" src="#"/>
													<img width="<?php echo $product['img-width']; ?>" height="<?php echo $product['img-height'] ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-secondary" data-src="<?php echo $product['additional_thumb']; ?>" src="#"/>
													<?php } else { ?>
													<img width="<?php echo $product['img-width']; ?>" height="<?php echo $product['img-height'] ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img" data-src="<?php echo $product['thumb']; ?>" src="#"/>
													<?php } ?>
												</a>												
												<?php if ($product['rating']) { ?>
												<div class="rating">
													<?php for ($i = 1; $i <= 5; $i++) { ?>
													<?php if ($product['rating'] < $i) { ?>
													<span class="fa-stack"><i class="material-icons-star fa-stack-1x"></i></span>
													<?php } else { ?>
													<span class="fa-stack"><i class="material-icons-star star fa-stack-1x"></i></span>
													<?php } ?>
													<?php } ?>
												</div>
												<?php } ?>
												<?php if ($product['special']) { ?>
												<?php if ($label_sale) { ?>
												<div class="sale">
													<span><?php echo $text_sale; ?></span>
												</div>
												<?php } ?>
												<?php if ($label_discount) { ?>
												<div class="discount">
													<span><?php echo $product['label_discount']; ?></span>
												</div>
												<?php } ?>
												<?php } ?>
												<?php if ($product['label_new']) { ?>
												<div class="new-pr"><span><?php echo $text_new; ?></span></div>
												<?php } ?>
											</div>
											<div class="caption">
												<div class="name">
													<a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
												</div>
												<div class="description">
													<?php if(strlen($product['description']) > 120) { echo substr($product['description'], 0, 120) . '..'; } else { echo $product['description']; } ?>
												</div>
												<?php if ($product['price']) { ?>
												<div class="price">
													<?php if (!$product['special']) { ?>
													<?php echo $product['price']; ?>
													<?php } else { ?>							 
													<span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
													<?php } ?>
													<?php if ($product['tax']) { ?>
													<span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
													<?php } ?>
												</div>
												<?php } ?>
												<div class="cart-button">
													<button class="btn-icon btn-add" type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" <?php if (count($product['options']) > 3) { ?> onclick="cart.add('<?php echo $product['product_id']; ?>');" <?php } else { ?> onclick="ajaxAdd($(this),<?php echo $product['product_id'] ?>);" <?php } ?>><i class="material-icons-shopping_cart"></i></button>
													<button class="btn-icon" type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart-o"></i></button>
													<a href="index.php?route=extension/module/tm_ajax_quick_view/ajaxQuickView" data-rel="details" data-product="<?php echo $product['product_id']; ?>" data-image-width="<?php echo $product['img-width']; ?>" data-image-height="<?php echo $product['img-height']; ?>" class="quickview btn-icon" data-toggle="tooltip" title="<?php echo $text_quick; ?>"><i class="material-icons-visibility"></i></a>
													<button class="btn-icon" type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="material-icons-repeat"></i></button>
												</div>
											</div>
										</div>
										<?php if ($layout_type == 0) { ?>
									</div>
									<?php } ?>
									<?php } ?>
								</div>
							</div>
						</div>
						<?php } ?>
						<?php if ($featured_products) { ?>
						<div role="tabpanel" class="tab-pane" id="tab-single-featured-<?php echo $module; ?>">
							<div class="box clearfix">
								<div class="<?php echo $layout_type ? 'box-carousel' : 'row mobile-carousel'; ?>">
									<?php $tf = 5000; $clr = 0;
									foreach ($featured_products as $product) { $tf++; $clr++; ?>
									<?php if ($layout_type == 0) { ?>
									<div class="product-layout col-lg-3 col-md-3 col-sm-3 col-xs-12" <?php echo $clr%4 == 0 ? 'data-clear=""' : ''; ?>>
										<?php } ?>
										<div class="product-thumb transition <?php if ($product['options']) echo 'options'; ?>">
											<?php if ($product['options']) { ?>
											<!-- Product options -->
											<div class="product-option-wrap">
												<div class="product-options form-horizontal">
													<div class="options">
														<a class="ajax-overlay_close" href='#'></a>
														<h3><?php echo $text_option; ?></h3>
														<div class="form-group hidden">
															<div class="col-sm-8">
																<input type="text" name="product_id" value="<?php echo $product['product_id'] ?>" class="form-control"/>
															</div>
														</div>
														<?php foreach ($product['options'] as $option) { ?>
														<?php if ($option['type'] == 'select') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] .  $module . $tf; ?>">
																<?php echo $option['name']; ?>
															</label>
															<div class="col-sm-12">
																<select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id'] .  $module . $tf; ?>" class="form-control">
																	<option value=""><?php echo $text_select; ?></option>
																	<?php foreach ($option['product_option_value'] as $option_value) { ?>
																	<option value="<?php echo $option_value['product_option_value_id']; ?>">
																		<?php echo $option_value['name']; ?>
																		<?php if ($option_value['price']) { ?>
																		(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
																		<?php } ?>
																	</option>
																	<?php } ?>
																</select>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'radio') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<div id="input-option<?php echo $option['product_option_id'] .  $module . $tf; ?>">
																	<?php foreach ($option['product_option_value'] as $option_value) { ?>
																	<div class="radio">
																		<label for="option[<?php echo $option['product_option_id'] . $option_value['product_option_value_id'] .  $module . $tf; ?>]">
																			<input type="radio" hidden name="option[<?php echo $option['product_option_id']; ?>]" id="option[<?php echo $option['product_option_id'] . $option_value['product_option_value_id'] .  $module . $tf; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>"/>
																			<?php echo $option_value['name']; ?>
																			<?php if ($option_value['price']) { ?>
																			(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
																			<?php } ?>
																		</label>
																	</div>
																	<?php } ?>
																</div>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'checkbox') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<div id="input-option<?php echo $option['product_option_id'] .  $module . $tf; ?>">
																	<?php foreach ($option['product_option_value'] as $option_value) { ?>
																	<div class="checkbox">
																		<label>
																			<input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>"/>
																			<?php echo $option_value['name']; ?>
																			<?php if ($option_value['price']) { ?>
																			(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
																			<?php } ?>
																		</label>
																	</div>
																	<?php } ?>
																</div>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'image') { ?>
														<div class="option-color form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<div id="input-option<?php echo $option['product_option_id'] .  $module . $tf; ?>">
																	<?php foreach ($option['product_option_value'] as $option_value) { ?>
																	<div class="radio">
																		<label>
											<input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
											<img width="21" height="21" data-toggle="tooltip" title="<?php echo $option_value['name']; ?><?php if ($option_value['price']) { ?> (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>) <?php } ?>" src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" />
										</label>
																	</div>
																	<?php } ?>
																</div>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'text') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] .  $module . $tf; ?>"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id'] .  $module . $tf; ?>" class="form-control"/>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'textarea') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] .  $module . $tf; ?>"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id'] .  $module . $tf; ?>" class="form-control">
																	<?php echo $option['value']; ?>
																</textarea>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'file') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12 "><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<button type="button" id="button-upload<?php echo $option['product_option_id'] .  $module . $tf; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-block btn-default">
																	<i class="fa fa-upload"></i>
																	<?php echo $button_upload; ?>
																</button>
																<input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id'] .  $module . $tf; ?>"/>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'date') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] .  $module . $tf; ?>"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<div class="input-group date">
																	<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id'] .  $module . $tf; ?>" class="form-control"/>
																	<span class="input-group-btn">
																		<button class="btn btn-default" type="button">
																			<i class="fa fa-calendar"></i>
																		</button>
																	</span>
																</div>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'datetime') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] .  $module . $tf; ?>"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<div class="input-group datetime">
																	<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id'] .  $module . $tf; ?>" class="form-control"/>
																	<span class="input-group-btn">
																		<button type="button" class="btn btn-default">
																			<i class="fa fa-calendar"></i>
																		</button>
																	</span>
																</div>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'time') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] .  $module . $tf; ?>">
																<?php echo $option['name']; ?>
															</label>
															<div class="col-sm-12">
																<div class="input-group time">
																	<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id'] .  $module . $tf; ?>" class="form-control"/>
																	<span class="input-group-btn">
																		<button type="button" class="btn btn-default">
																			<i class="fa fa-calendar"></i>
																		</button>
																	</span>
																</div>
															</div>
														</div>
														<?php } ?>
														<?php } ?>
														<button class="btn-primary" type="button" onclick="cart.addPopup($(this),'<?php echo $product['product_id']; ?>');"><?php echo $button_cart; ?></button>
													</div>
												</div>
											</div>
											<?php } ?>											
											
											<div class="image">
												<a class="lazy" style="padding-bottom: <?php echo($product['img-height'] / $product['img-width'] * 100); ?>%" href="<?php echo $product['href']; ?>">
													<?php if ($product['additional_thumb']) { ?>
													<img width="<?php echo $product['img-width']; ?>" height="<?php echo $product['img-height'] ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-primary" data-src="<?php echo $product['thumb']; ?>" src="#"/>
													<img width="<?php echo $product['img-width']; ?>" height="<?php echo $product['img-height'] ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-secondary" data-src="<?php echo $product['additional_thumb']; ?>" src="#"/>
													<?php } else { ?>
													<img width="<?php echo $product['img-width']; ?>" height="<?php echo $product['img-height'] ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img" data-src="<?php echo $product['thumb']; ?>" src="#"/>
													<?php } ?>
												</a>												
												<?php if ($product['rating']) { ?>
												<div class="rating">
													<?php for ($i = 1; $i <= 5; $i++) { ?>
													<?php if ($product['rating'] < $i) { ?>
													<span class="fa-stack"><i class="material-icons-star fa-stack-1x"></i></span>
													<?php } else { ?>
													<span class="fa-stack"><i class="material-icons-star star fa-stack-1x"></i></span>
													<?php } ?>
													<?php } ?>
												</div>
												<?php } ?>
												<?php if ($product['special']) { ?>
												<?php if ($label_sale) { ?>
												<div class="sale">
													<span><?php echo $text_sale; ?></span>
												</div>
												<?php } ?>
												<?php if ($label_discount) { ?>
												<div class="discount">
													<span><?php echo $product['label_discount']; ?></span>
												</div>
												<?php } ?>
												<?php } ?>
												<?php if ($product['label_new']) { ?>
												<div class="new-pr"><span><?php echo $text_new; ?></span></div>
												<?php } ?>
											</div>
											<div class="caption">
												<div class="name">
													<a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
												</div>
												<div class="description">
													<?php if(strlen($product['description']) > 120) { echo substr($product['description'], 0, 120) . '..'; } else { echo $product['description']; } ?>
												</div>
												<?php if ($product['price']) { ?>
												<div class="price">
													<?php if (!$product['special']) { ?>
													<?php echo $product['price']; ?>
													<?php } else { ?>							 
													<span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
													<?php } ?>
													<?php if ($product['tax']) { ?>
													<span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
													<?php } ?>
												</div>
												<?php } ?>
												<div class="cart-button">
													<button class="btn-icon btn-add" type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" <?php if (count($product['options']) > 3) { ?> onclick="cart.add('<?php echo $product['product_id']; ?>');" <?php } else { ?> onclick="ajaxAdd($(this),<?php echo $product['product_id'] ?>);" <?php } ?>><i class="material-icons-shopping_cart"></i></button>
													<button class="btn-icon" type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart-o"></i></button>
													<a href="index.php?route=extension/module/tm_ajax_quick_view/ajaxQuickView" data-rel="details" data-product="<?php echo $product['product_id']; ?>" data-image-width="<?php echo $product['img-width']; ?>" data-image-height="<?php echo $product['img-height']; ?>" class="quickview btn-icon" data-toggle="tooltip" title="<?php echo $text_quick; ?>"><i class="material-icons-visibility"></i></a>
													<button class="btn-icon" type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="material-icons-repeat"></i></button>
												</div>
											</div>
										</div>
										<?php if ($layout_type == 0) { ?>
									</div>
									<?php } ?>
									<?php } ?>
								</div>
							</div>
						</div>
						<?php } ?>
						<?php if ($special_products) { ?>
						<div role="tabpanel" class="tab-pane" id="tab-single-specials-<?php echo $module; ?>">
							<div class="box clearfix">
								<div class="<?php echo $layout_type ? 'box-carousel' : 'row mobile-carousel'; ?>">
									<?php $ts = 7000; $clr = 0;
									foreach ($special_products as $product) { $ts++; $clr++; ?>
									<?php if ($layout_type == 0) { ?>
									<div class="product-layout col-lg-3 col-md-3 col-sm-3 col-xs-12" <?php echo $clr%4 == 0 ? 'data-clear=""' : ''; ?>>
										<?php } ?>
										<div class="product-thumb transition <?php if ($product['options']) echo 'options'; ?>">
											<?php if ($product['options']) { ?>
											<!-- Product options -->
											<div class="product-option-wrap">
												<div class="product-options form-horizontal">
													<div class="options">
														<a class="ajax-overlay_close" href='#'></a>
														<h3><?php echo $text_option; ?></h3>
														<div class="form-group hidden">
															<div class="col-sm-8">
																<input type="text" name="product_id" value="<?php echo $product['product_id'] ?>" class="form-control"/>
															</div>
														</div>
														<?php foreach ($product['options'] as $option) { ?>
														<?php if ($option['type'] == 'select') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] .  $module . $ts; ?>">
																<?php echo $option['name']; ?>
															</label>
															<div class="col-sm-12">
																<select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id'] .  $module . $ts; ?>" class="form-control">
																	<option value=""><?php echo $text_select; ?></option>
																	<?php foreach ($option['product_option_value'] as $option_value) { ?>
																	<option value="<?php echo $option_value['product_option_value_id']; ?>">
																		<?php echo $option_value['name']; ?>
																		<?php if ($option_value['price']) { ?>
																		(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
																		<?php } ?>
																	</option>
																	<?php } ?>
																</select>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'radio') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<div id="input-option<?php echo $option['product_option_id'] .  $module . $ts; ?>">
																	<?php foreach ($option['product_option_value'] as $option_value) { ?>
																	<div class="radio">
																		<label for="option[<?php echo $option['product_option_id'] . $option_value['product_option_value_id'] .  $module . $ts; ?>]">
																			<input type="radio" hidden name="option[<?php echo $option['product_option_id']; ?>]" id="option[<?php echo $option['product_option_id'] . $option_value['product_option_value_id'] .  $module . $ts; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>"/>
																			<?php echo $option_value['name']; ?>
																			<?php if ($option_value['price']) { ?>
																			(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
																			<?php } ?>
																		</label>
																	</div>
																	<?php } ?>
																</div>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'checkbox') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<div id="input-option<?php echo $option['product_option_id'] .  $module . $ts; ?>">
																	<?php foreach ($option['product_option_value'] as $option_value) { ?>
																	<div class="checkbox">
																		<label>
																			<input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>"/>
																			<?php echo $option_value['name']; ?>
																			<?php if ($option_value['price']) { ?>
																			(<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
																			<?php } ?>
																		</label>
																	</div>
																	<?php } ?>
																</div>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'image') { ?>
														<div class="option-color form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<div id="input-option<?php echo $option['product_option_id'] .  $module . $ts; ?>">
																	<?php foreach ($option['product_option_value'] as $option_value) { ?>
																	<div class="radio">
																		<label>
											<input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
											<img width="21" height="21" data-toggle="tooltip" title="<?php echo $option_value['name']; ?><?php if ($option_value['price']) { ?> (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>) <?php } ?>" src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" />
										</label>
																	</div>
																	<?php } ?>
																</div>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'text') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] .  $module . $ts; ?>"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id'] .  $module . $ts; ?>" class="form-control"/>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'textarea') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] .  $module . $ts; ?>"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id'] .  $module . $ts; ?>" class="form-control">
																	<?php echo $option['value']; ?>
																</textarea>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'file') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12 "><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<button type="button" id="button-upload<?php echo $option['product_option_id'] .  $module . $ts; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-block btn-default">
																	<i class="fa fa-upload"></i>
																	<?php echo $button_upload; ?>
																</button>
																<input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id'] .  $module . $ts; ?>"/>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'date') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] .  $module . $ts; ?>"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<div class="input-group date">
																	<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id'] .  $module . $ts; ?>" class="form-control"/>
																	<span class="input-group-btn">
																		<button class="btn btn-default" type="button">
																			<i class="fa fa-calendar"></i>
																		</button>
																	</span>
																</div>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'datetime') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] .  $module . $ts; ?>"><?php echo $option['name']; ?></label>
															<div class="col-sm-12">
																<div class="input-group datetime">
																	<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id'] .  $module . $ts; ?>" class="form-control"/>
																	<span class="input-group-btn">
																		<button type="button" class="btn btn-default">
																			<i class="fa fa-calendar"></i>
																		</button>
																	</span>
																</div>
															</div>
														</div>
														<?php } ?>
														<?php if ($option['type'] == 'time') { ?>
														<div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
															<label class="control-label col-sm-12" for="input-option<?php echo $option['product_option_id'] .  $module . $ts; ?>">
																<?php echo $option['name']; ?>
															</label>
															<div class="col-sm-12">
																<div class="input-group time">
																	<input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id'] .  $module . $ts; ?>" class="form-control"/>
																	<span class="input-group-btn">
																		<button type="button" class="btn btn-default">
																			<i class="fa fa-calendar"></i>
																		</button>
																	</span>
																</div>
															</div>
														</div>
														<?php } ?>
														<?php } ?>
														<button class="btn-primary" type="button" onclick="cart.addPopup($(this),'<?php echo $product['product_id']; ?>');"><?php echo $button_cart; ?></button>
													</div>
												</div>
											</div>
											<?php } ?>											
											
											<div class="image">
												<a class="lazy" style="padding-bottom: <?php echo($product['img-height'] / $product['img-width'] * 100); ?>%" href="<?php echo $product['href']; ?>">
													<?php if ($product['additional_thumb']) { ?>
													<img width="<?php echo $product['img-width']; ?>" height="<?php echo $product['img-height'] ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-primary" data-src="<?php echo $product['thumb']; ?>" src="#"/>
													<img width="<?php echo $product['img-width']; ?>" height="<?php echo $product['img-height'] ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-secondary" data-src="<?php echo $product['additional_thumb']; ?>" src="#"/>
													<?php } else { ?>
													<img width="<?php echo $product['img-width']; ?>" height="<?php echo $product['img-height'] ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img" data-src="<?php echo $product['thumb']; ?>" src="#"/>
													<?php } ?>
												</a>												
												<?php if ($product['rating']) { ?>
												<div class="rating">
													<?php for ($i = 1; $i <= 5; $i++) { ?>
													<?php if ($product['rating'] < $i) { ?>
													<span class="fa-stack"><i class="material-icons-star fa-stack-1x"></i></span>
													<?php } else { ?>
													<span class="fa-stack"><i class="material-icons-star star fa-stack-1x"></i></span>
													<?php } ?>
													<?php } ?>
												</div>
												<?php } ?>
												<?php if ($product['special']) { ?>
												<?php if ($label_sale) { ?>
												<div class="sale">
													<span><?php echo $text_sale; ?></span>
												</div>
												<?php } ?>
												<?php if ($label_discount) { ?>
												<div class="discount">
													<span><?php echo $product['label_discount']; ?></span>
												</div>
												<?php } ?>
												<?php } ?>
												<?php if ($product['label_new']) { ?>
												<div class="new-pr"><span><?php echo $text_new; ?></span></div>
												<?php } ?>
											</div>
											<div class="caption">
												<div class="name">
													<a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
												</div>
												<div class="description">
													<?php if(strlen($product['description']) > 120) { echo substr($product['description'], 0, 120) . '..'; } else { echo $product['description']; } ?>
												</div>
												<?php if ($product['price']) { ?>
												<div class="price">
													<?php if (!$product['special']) { ?>
													<?php echo $product['price']; ?>
													<?php } else { ?>							 
													<span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
													<?php } ?>
													<?php if ($product['tax']) { ?>
													<span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
													<?php } ?>
												</div>
												<?php } ?>
												<div class="cart-button">
													<button class="btn-icon btn-add" type="button" data-toggle="tooltip" title="<?php echo $button_cart; ?>" <?php if (count($product['options']) > 3) { ?> onclick="cart.add('<?php echo $product['product_id']; ?>');" <?php } else { ?> onclick="ajaxAdd($(this),<?php echo $product['product_id'] ?>);" <?php } ?>><i class="material-icons-shopping_cart"></i></button>
													<button class="btn-icon" type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart-o"></i></button>
													<a href="index.php?route=extension/module/tm_ajax_quick_view/ajaxQuickView" data-rel="details" data-product="<?php echo $product['product_id']; ?>" data-image-width="<?php echo $product['img-width']; ?>" data-image-height="<?php echo $product['img-height']; ?>" class="quickview btn-icon" data-toggle="tooltip" title="<?php echo $text_quick; ?>"><i class="material-icons-visibility"></i></a>
													<button class="btn-icon" type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="material-icons-repeat"></i></button>
												</div>
											</div>											
										</div>
										<?php if ($layout_type == 0) { ?>
									</div>
									<?php } ?>
									<?php } ?>
								</div>
							</div>
						</div>
						<?php } ?>
						<div class="view-all">
							<a href='<?php echo $category_link ?>'><?php echo $text_view; ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>

