<?php if ($articles) { ?>
<div class="box blog_articles">
	<div class="box-heading">
		<h3><?php echo $heading_title; ?></h3>
	</div>
	<div class="box-content">
		<div class="row mobile-carousel">
			<?php foreach ($articles as $article) { ?>
			<div class="col-sm-4">
			<div>
				<?php if ($show_image == 1) { ?>
				<figure class="article-image">
					<a href="<?php echo $article['href']; ?>"><img width="<?php echo $article['image_width']; ?>" height="<?php echo $article['image_height']; ?>" src="<?php echo $article['image']; ?>" alt="<?php echo $article['article_title'] ?>"/></a>
				</figure>
				<?php } ?>
					<div class="tx-block">
						<div class="article-title">
							<a href="<?php echo $article['href']; ?>"><?php echo $article['article_title'] ?></a>
						</div>											
					</div>
					<?php if ($show_date == 1 || $show_author == 1 || $show_comments == 1) { ?>					
						<div class="article-sub-title">				
								<span class="article-date">
									<?php if ($show_date == 1) { ?>
										<?php echo $text_posted; ?> <?php echo $article['date_added']; ?>
									<?php } ?>
									<?php  if ($show_author == 1) { ?>
										<?php echo $text_by; ?>  <a href="<?php echo $article['author_href']; ?>"><?php echo $article['author_name']; ?></a>
									<?php } ?>
								</span>						
							<?php if ($article['allow_comment'] && $show_comments == 1) { ?>
								<span class="article-comments material-icons-insert_comment">
									<a href="<?php echo $article['comment_href']; ?>"><?php echo $article['total_comment']; ?></a>
								</span>
							<?php } ?>
						</div>
					<?php } ?>
					<?php if ($show_description && $article['description']) { ?>
						<div class="article-description"><?php echo $article['description']; ?></div>
					<?php } ?>	
					<?php if ($show_readmore == 1) { ?>
						<a href='<?php echo $article['href']; ?>' class="btn-primary"><?php echo $text_button_continue?></a>
					<?php } ?>					
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</div>
<?php } else { ?>
<div class="buttons">
	<div class="center"><?php echo $text_no_found; ?></div>
</div>
<?php } ?>