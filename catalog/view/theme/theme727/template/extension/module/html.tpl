<div class="html-module <?php echo $heading_title; ?>">
	<?php if($heading_title) { ?>
		<div class="box-heading"><h3><?php echo $heading_title; ?></h3></div>
	<?php } ?>	
	<div class="content"><?php echo $html; ?></div>
</div>
