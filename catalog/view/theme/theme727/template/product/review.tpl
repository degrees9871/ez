<?php if ($reviews) { ?>
<?php foreach ($reviews as $review) { ?>
<div class="review-item">
		<div class="review-score">
			<?php for ($i = 1; $i <= 5; $i++) { ?>
			<?php if ($review['rating'] < $i) { ?>
			<span class="fa-stack"><i class="material-icons-star fa-stack-1x"></i></span>
			<?php } else { ?>
			<span class="fa-stack"><i class="material-icons-star star fa-stack-1x"></i></span>
			<?php } ?>
			<?php } ?>
		</div>		
		<div class="review-date"><?php echo $review['date_added']; ?></div>
		<p><?php echo $review['text']; ?></p>
		<div class="review-author"><i class="material-icons-person"></i><?php echo $review['author']; ?></div>		
</div>
<?php } ?>
<div class="text-right"><?php echo $pagination; ?></div>
<?php } else { ?>
<p><?php echo $text_no_reviews; ?></p>
<?php } ?>
