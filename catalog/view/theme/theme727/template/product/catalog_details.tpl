<?php if ($product['special']) { ?>
<?php if ($label_sale) { ?>
<div class="sale">
	<span><?php echo $text_sale; ?></span>
</div>
<?php } ?>
<?php if ($label_discount) { ?>
<div class="discount">
	<span><?php echo $product['label_discount']; ?></span>
</div>
<?php } ?>
<?php } ?>
<?php if ($product['label_new']) { ?>
<div class="new-pr"><span><?php echo $text_new; ?></span></div>
<?php } ?>
<div class="name">
	<a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
</div>
<?php if ($product['price']) { ?>
<div class="price price-product">
	<?php if (!$product['special']) { ?>
	<?php echo $product['price']; ?>
	<?php } else { ?>
	<span class="price-new"><?php echo $product['special']; ?></span>
	<span class="price-old"><?php echo $product['price']; ?></span>
	<?php } ?>
	<?php if ($product['tax']) { ?>
	<span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
	<?php } ?>
</div>
<?php } ?>
<?php if ($product['rating']) { ?>
<div class="rating">
	<?php for ($i = 1; $i <= 5; $i++) { ?>
	<?php if ($product['rating'] < $i) { ?>
	<span class="fa-stack"><i class="material-icons-star fa-stack-1x"></i></span>
	<?php } else { ?>
	<span class="fa-stack"><i class="material-icons-star star fa-stack-1x"></i></span>
	<?php } ?>
	<?php } ?>
</div>
<?php } ?>
<?php if ($product['options']) { ?>
<div class="options">
	<h3><?php echo $text_option; ?></h3>
	<?php foreach ($product['options'] as $option) { ?>
	<?php if ($option['type'] == 'select' || $option['type'] == 'radio' || $option['type'] == 'checkbox' || $option['type'] == 'image') { ?>
	<div class="option">
		<div class="name"><?php echo $option['name']; ?>:</div>
		<ul>
			<?php foreach ($option['product_option_value'] as $option_value) { ?>
			<li>
				<?php if ($option['type'] == 'image') { ?>
				<img width="50" height="50" src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail"/>
				<?php } ?>
				<span>
					<?php echo $option_value['name']; ?></span>
				</li>
				<?php } ?>
			</ul>
		</div>
		<?php } ?>
		<?php } ?>
	</div>
	<?php } ?>
	<?php if ($product['attributes']) { ?>
	<div class="product-spec product-section">
		<table class="table table-bordered">
			<?php foreach ($product['attributes'] as $attribute_group) { ?>
			<thead>
				<tr>
					<th colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($attribute_group['attribute'] as $attribute) { ?>
				<tr>
					<td><?php echo $attribute['name']; ?></td>
					<td><?php echo $attribute['text']; ?></td>
				</tr>
				<?php } ?>
			</tbody>
			<?php } ?>
		</table>
	</div>
	<?php } ?>
	<div class="description">
		<?php echo $product['description']; ?>
	</div>