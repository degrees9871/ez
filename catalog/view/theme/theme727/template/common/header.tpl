<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<?php $page_direction == 'rtl' ? $direction = 'rtl' : ''; ?>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="<?php echo $responsive ? 'mobile-responsive-off' : ''; ?>">
<!--<![endif]-->
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $title; ?></title>
	<base href="<?php echo $base; ?>" />
	<?php if ($description) { ?>
	<meta name="description" content="<?php echo $description; ?>" />
	<?php } ?>
	<?php if ($keywords) { ?>
	<meta name="keywords" content= "<?php echo $keywords; ?>" />
	<?php } ?>
	<?php foreach ($add_this_meta as $meta) { ?>
	<meta property="og:<?php echo $meta['property'] ?>" content="<?php echo $meta['content'] ?>" /> 
	<?php } ?>
	<!-- Fonts -->
	<link href="//fonts.googleapis.com/css?family=Open+Sans:400,600,700&amp;subset=cyrillic" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Raleway:400,600,700" rel="stylesheet">
	
	<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<!-- END Fonts -->
	<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />

	<?php if ($direction == 'rtl') { ?>
	<link href="catalog/view/theme/<?php echo $theme_path; ?>/stylesheet/bootstrap-rtl.css" rel="stylesheet">
	<?php } ?>

	<link href="catalog/view/theme/<?php echo $theme_path; ?>/js/owl.carousel/assets/owl.carousel.css" rel="stylesheet">
	<link href="catalog/view/theme/<?php echo $theme_path; ?>/stylesheet/material-design.css" rel="stylesheet">
	<link href="catalog/view/theme/<?php echo $theme_path; ?>/stylesheet/material-icons.css" rel="stylesheet">
	<link href="catalog/view/theme/<?php echo $theme_path; ?>/stylesheet/fl-outicons.css" rel="stylesheet">
	<link href="catalog/view/theme/<?php echo $theme_path; ?>/js/fancybox/jquery.fancybox.css" rel="stylesheet">
	<link href="catalog/view/theme/<?php echo $theme_path; ?>/stylesheet/photoswipe.css" rel="stylesheet">
	<link href="catalog/view/theme/<?php echo $theme_path; ?>/stylesheet/stylesheet.css" rel="stylesheet">

	<?php if ($direction == 'rtl') { ?>
	<link href="catalog/view/theme/<?php echo $theme_path; ?>/stylesheet/rtl.css" rel="stylesheet">
	<?php } ?>

	<?php if (isset($_COOKIE['tm_color_switcher_scheme']) && !$tm_color_switcher_permission) { ?>
	<link id="color_scheme" href="catalog/view/theme/<?php echo $theme_path; ?>/stylesheet/color_schemes/<?php echo $_COOKIE['tm_color_switcher_scheme'] . '.css'; ?>" rel="stylesheet">
	<?php } elseif ($tm_color_switcher_scheme) { ?>
	<link id="color_scheme" href="catalog/view/theme/<?php echo $theme_path; ?>/stylesheet/color_schemes/<?php echo $tm_color_switcher_scheme . '.css'; ?>" rel="stylesheet">
	<?php } ?>

	<?php foreach ($styles as $style) { ?>
	<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
	<?php } ?>
	<?php foreach ($links as $link) { ?>
	<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
	<?php } ?>

	<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
	<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

	<?php foreach ($scripts as $script) { ?>
	<script src="<?php echo $script; ?>" type="text/javascript"></script>
	<?php } ?>
	<?php foreach ($analytics as $analytic) { ?>
	<?php echo $analytic; ?>
	<?php } ?>
</head>
<body class="<?php echo $class; ?>">
	<p id="gl_path" class="hidden"><?php echo $theme_path; ?></p>
	<div id="page">
		<div id="page-preloader" class="visible"><span class="preloader"></span></div>
		<div class="ie-warning">
			<a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
				<img src="catalog/view/theme/<?php echo $theme_path; ?>/image/warning_bar_0000_us.jpg" border="0" height="75" width="1170" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
			</a>
		</div>
		<header>
			<?php if ($header_top) { ?>
				<div class="header-top"> <?php echo $header_top; ?></div>
			<?php } ?>
			
			<?php if ($stuck_menu) { ?>
				<div id="stuck" class="stuck-menu"> <div class="container"><?php echo $stuck_menu; ?> </div> </div>
			<?php } ?>
			
			<?php if ($navigation) { ?>
				<div class="navigation"> <div class="container"> <?php echo $navigation;?> </div> </div>
			<?php } ?>
		</header>