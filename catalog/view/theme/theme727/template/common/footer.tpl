<footer>
	<div class="container">
		<div class="row">
			<?php if ($footer_top) { ?>
			<div class="container">
				<div class="footer_modules">
					<?php echo $footer_top; ?>
				</div>
			</div>
			<?php } ?>			
			<?php if ($footer_accordion) { ?>
			<div class="accordion-footer container">
				<?php echo $footer_accordion; ?>
			</div>
			<?php } ?>
		</div>
	</div>	
	<!-- <div class="copyright">
		<!-- <div class="container"> <?php echo $powered; ?>[[%FOOTER_LINK]] </div>
	</div>  -->
</footer>
<div class="ajax-overlay"></div>
<div class="ajax-quickview-overlay">
	<span class="ajax-quickview-overlay__preloader"></span>
</div>
</div>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" type="text/javascript"></script>
<?php } ?>
<script src="catalog/view/theme/<?php echo $theme_path; ?>/js/device.min.js" type="text/javascript"></script>
<script src="catalog/view/theme/<?php echo $theme_path; ?>/js/livesearch.min.js" type="text/javascript"></script>
<script src="catalog/view/theme/<?php echo $theme_path; ?>/js/common.js" type="text/javascript"></script>
<script src="catalog/view/theme/<?php echo $theme_path; ?>/js/script.js" type="text/javascript"></script>
<!-- coding by xena -->
</body></html>