<ul class="social-list list-unstyled">
	<?php foreach ($socials as $social) { ?>
	<li class="<?php echo $social['css']; ?>"><a href="<?php echo $social['link']; ?>"><?php echo $social['name']; ?></a></li>
	<?php } ?>
</ul>