<?php
class ModelExtensionTotalAbcCustomer extends Model {
	public function getTotal($total) {
		static $total_result = 0;
		static $shipping_rate = 0;
		if($user_id = $this->customer->isLogged()){
			$this->load->model('account/customer');
			$this->load->model('catalog/product');
			$customer = $this->model_account_customer->getCustomer($user_id);
			$discounts = $this->config->get('abc_customer_group_id');
			if($total_result == 0) {
				$products = $this->cart->getProducts();
				foreach ($products as $product) {
					$total_result += $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity'];
				}
			}
			if (isset($this->session->data['shipping_address'])) {
				$this->load->model('extension/extension');
				$results_shipping = $this->model_extension_extension->getExtensions('shipping');
				foreach ($results_shipping as $result_shipping) {
					if ($this->config->get($result_shipping['code'] . '_status')) {
						$this->load->model('extension/shipping/' . $result_shipping['code']);
						$quote = $this->{'model_extension_shipping_' . $result_shipping['code']}->getQuote($this->session->data['shipping_address']);
						$shipping_rate = floatval($quote['quote']['flat']['cost']);
					}
				}
			}
			foreach ($discounts as $group_id => $discount){
				if($group_id == $customer['customer_group_id']){
					$this->load->language('extension/total/abc_customer');
					$subtraction = $total_result*($discount/100);
					$result = $total_result - $subtraction + $shipping_rate;
					$total['totals'][] = array(
						'code'       => 'abc_customer',
						'title'      => sprintf($this->language->get('text_total_discount'), -$discount),
						'text'       => $text = $this->currency->format($result, $this->session->data['currency']),
						'value'      => $result,
						'sort_order' => $this->config->get('total_sort_order')
					);
				}
			}
		}
	}
}
?>