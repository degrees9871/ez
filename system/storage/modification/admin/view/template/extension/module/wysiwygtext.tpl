<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-wysiwygtext" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-wysiwygtext" class="form-horizontal">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-editor"><?php echo $entry_editor; ?></label>
            <div class="col-sm-10">
              <select name="wysiwygtext_editor" id="input-editor" class="form-control">
                <?php if ($wysiwygtext_editor == 'tinymce') { ?>
                <option value="tinymce" selected="selected">TinyMCE 4.9.2</option>
                <option value="ckeditor">CKEditor 5</option>
                <?php } else { ?>
                <option value="tinymce">Tiny MCE 4.9.2</option>
                <option value="ckeditor" selected="selected">CKEditor 5</option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-uploadmethod"><?php echo $entry_uploadmethod; ?></label>
            <div class="col-sm-10">
              <select name="wysiwygtext_uploadmethod" id="input-uploadmethod" class="form-control">
                <?php foreach ($uploadmethods as $key => $method) { ?>
                  <option value="<?php echo $key; ?>" <?php echo $key == $wysiwygtext_uploadmethod ? 'selected="selected"' : ''?>><?php echo $method; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-uploaddir"><span data-toggle="tooltip" title="<?php echo $help_uploaddir; ?>"><?php echo $entry_uploaddir; ?></span></label>
            <div class="col-sm-10">
              <input type="text" name="wysiwygtext_uploaddir" value="<?php echo $wysiwygtext_uploaddir; ?>" placeholder="<?php echo $entry_uploaddir; ?>" id="input-uploaddir" class="form-control" />
            </div>
          </div>
		  <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="wysiwygtext_status" id="input-status" class="form-control">
                <?php if ($wysiwygtext_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('select[name=\'wysiwygtext_uploadmethod\']').on('change', function() {
	if (this.value == 'standart') {
		$('#input-uploaddir').parent().parent().hide();
	} else {
		$('#input-uploaddir').parent().parent().show();
	}
});
$('select[name=\'wysiwygtext_uploadmethod\']').trigger('change');
</script>

				<script>
					$(document).on("submit","form",function(e){
						summernotes = $('.summernote');
						$.each(summernotes, function(){
							if ($(this).summernote('codeview.isActivated')) {
								$(this).summernote('codeview.deactivate'); 
							}
						})
					})
				</script>
<?php echo $footer; ?>