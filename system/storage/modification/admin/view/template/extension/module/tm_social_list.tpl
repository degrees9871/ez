<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button type="submit" form="form-social-list" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary">
					<i class="fa fa-save"></i>
				</button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default">
					<i class="fa fa-reply"></i>
				</a>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
				<li>
					<a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
				</li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
		<?php if ($error_warning) { ?>
		<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
			<button type="button" class="close" data-dismiss="alert">&times;</button>
		</div>
		<?php } ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-social-list" class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-name"><?php echo $entry_name; ?></label>
						<div class="col-sm-10">
							<input type="text" name="name" value="<?php echo $name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control"/>
							<?php if ($error_name) { ?>
							<div class="text-danger"><?php echo $error_name; ?></div>
							<?php } ?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
						<div class="col-sm-10">
							<select name="status" id="input-status" class="form-control">
								<?php if ($status) { ?>
								<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
								<option value="0"><?php echo $text_disabled; ?></option>
								<?php } else { ?>
								<option value="1"><?php echo $text_enabled; ?></option>
								<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="tab-pane">
						<ul class="nav nav-tabs" id="language">
							<?php foreach ($languages as $language) { ?>
							<li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
							<?php } ?>
						</ul>
						<div class="tab-content">
							<?php foreach ($languages as $language) { ?>
							<div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
								<fieldset>
									<legend><?php echo $entry_social_info; ?></legend>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="input-title<?php echo $language['language_id']; ?>"><?php echo $entry_title; ?></label>
										<div class="col-sm-10">
											<input type="text" name="title[<?php echo $language['language_id']; ?>]" value="<?php echo $title[$language['language_id']]; ?>" placeholder="<?php echo $entry_name; ?>" id="input-title<?php echo $language['language_id']; ?>" class="form-control"/>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
										<div class="col-sm-10">
											<textarea name="description[<?php echo $language['language_id']; ?>]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>" class="form-control summernote">
												<?php echo isset($description[$language['language_id']]) ? $description[$language['language_id']] : ''; ?>
											</textarea>
										</div>
									</div>
								</fieldset>
								<fieldset class="socials">
									<legend><?php echo $entry_social_icons; ?></legend>
									<?php if (isset($socials[$language['language_id']])) { ?>
									<?php foreach ($socials[$language['language_id']] as $social_id => $social) { ?>
									<div class="form-group social-info-<?php echo $social_id; ?>">
										<div class="col-lg-4">
											<div class="row">
												<label class="col-sm-4 control-label" for="input-social-name-<?php echo $language['language_id'] . '-' . $social_id; ?>"><?php echo $entry_social_name; ?></label>
												<div class="col-sm-8">
													<input type="text" name="socials[<?php echo $language['language_id'] ?>][<?php echo $social_id; ?>][name]" value="<?php echo isset($socials[$language['language_id']][$social_id]['name']) ? $socials[$language['language_id']][$social_id]['name'] : ''; ?>" placeholder="<?php echo $entry_social_name; ?>" id="input-social-name-<?php echo $language['language_id'] . '-' . $social_id; ?>" class="form-control"/>
												</div>
											</div>
										</div>
										<div class="col-lg-4">
											<div class="row">
												<label class="col-sm-4 control-label" for="input-social-link-<?php echo $language['language_id'] . '-' . $social_id; ?>"><?php echo $entry_social_link; ?></label>
												<div class="col-sm-8">
													<input type="text" name="socials[<?php echo $language['language_id'] ?>][<?php echo $social_id; ?>][link]" value="<?php echo isset($socials[$language['language_id']][$social_id]['link']) ? $socials[$language['language_id']][$social_id]['link'] : ''; ?>" placeholder="<?php echo $entry_social_link; ?>" id="input-social-link-<?php echo $language['language_id'] . '-' . $social_id; ?>" class="form-control"/>
												</div>
											</div>
										</div>
										<div class="col-lg-3">
											<div class="row">
												<label class="col-sm-4 control-label" for="input-social-css-<?php echo $language['language_id'] . '-' . $social_id; ?>"><?php echo $entry_social_css; ?></label>
												<div class="col-sm-8">
													<input type="text" name="socials[<?php echo $language['language_id'] ?>][<?php echo $social_id; ?>][css]" value="<?php echo isset($socials[$language['language_id']][$social_id]['css']) ? $socials[$language['language_id']][$social_id]['css'] : ''; ?>" placeholder="<?php echo $entry_social_css; ?>" id="input-social-css-<?php echo $language['language_id'] . '-' . $social_id; ?>" class="form-control"/>
												</div>
											</div>
										</div>
										<div class="col-lg-1 text-right">
											<button type="button" onclick="$('.social-info-<?php echo $social_id; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove_social; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
										</div>
									</div>
									<?php } ?>
									<?php } ?>
								</fieldset>
							</div>
							<?php } ?>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12 text-right">
							<button type="button" onclick="addSocial();" data-toggle="tooltip" title="<?php echo $button_add_social; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function addSocial() {

		var socialCount = $('[class*="social-info"]').length / 2 + 1;

		$('.socials').each(function(language_id){
			language_id += 1;
			var html = '<div class="form-group social-info-' + socialCount + '">';
			html += '	<div class="col-lg-4">';
			html += '		<div class="row">';
			html += '			<label class="col-sm-4 control-label" for="input-social-name-' + language_id + '-' + socialCount + '"><?php echo $entry_social_name; ?></label>';
			html += '			<div class="col-sm-8">';
			html += '				<input type="text" name="socials[' + language_id + '][' + socialCount + '][name]" value="" placeholder="<?php echo $entry_social_name; ?>" id="input-social-name-' + language_id + '-' + socialCount + '" class="form-control"/>';
			html += '			</div>';
			html += '		</div>';
			html += '	</div>';
			html += '	<div class="col-lg-4">';
			html += '		<div class="row">';
			html += '			<label class="col-sm-4 control-label" for="input-social-link-' + language_id + '-' + socialCount + '"><?php echo $entry_social_link; ?></label>';
			html += '			<div class="col-sm-8">';
			html += '				<input type="text" name="socials[' + language_id + '][' + socialCount + '][link]" value="" placeholder="<?php echo $entry_social_link; ?>" id="input-social-link-' + language_id + '-' + socialCount + '" class="form-control"/>';
			html += '			</div>';
			html += '		</div>';
			html += '	</div>';
			html += '	<div class="col-lg-3">';
			html += '		<div class="row">';
			html += '			<label class="col-sm-4 control-label" for="input-social-css-' + language_id + '-' + socialCount + '"><?php echo $entry_social_css; ?></label>';
			html += '			<div class="col-sm-8">';
			html += '				<input type="text" name="socials[' + language_id + '][' + socialCount + '][css]" value="" placeholder="<?php echo $entry_social_css; ?>" id="input-social-css-' + language_id + '-' + socialCount + '" class="form-control"/>';
			html += '			</div>';
			html += '		</div>';
			html += '	</div>';
			html += '	<div class="col-lg-1 text-right">';
			html += '		<button type="button" onclick="$(\'.social-info-' + socialCount + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove_social; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>';
			html += '	</div>';
			html += '</div>';
			$(this).append(html);
		});
	}
</script>
<script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
<link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
<script type="text/javascript" src="view/javascript/summernote/opencart.js"></script>
<script type="text/javascript">
	$('#language a:first').tab('show');
</script>

				<script>
					$(document).on("submit","form",function(e){
						summernotes = $('.summernote');
						$.each(summernotes, function(){
							if ($(this).summernote('codeview.isActivated')) {
								$(this).summernote('codeview.deactivate'); 
							}
						})
					})
				</script>
<?php echo $footer; ?>